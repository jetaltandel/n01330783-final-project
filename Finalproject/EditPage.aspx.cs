﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Finalproject
{

    //Reference from class example

    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                pageContent.InnerHtml = "No Page Found.";
                return;
            }
            page_title.Text = pagerow["pagetitle"].ToString();
            page_content.Text = pagerow["pagecontent"].ToString();

        }

        protected void Edit_Page(object sender, EventArgs e)
        {
            
            string editpagetitle = page_title.Text;
            string editpagecontent = page_content.Text;
           


            string pageeditquery = "Update pagedata_base set pagetitle='" + editpagetitle + "'," +
                " pagecontent='" + editpagecontent + "'" +
                "where pageid=" + pageid;


            edit_page.UpdateCommand = pageeditquery;
            edit_page.Update();
        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pagedata_base where pageid=" + pageid.ToString();
            page_select.SelectCommand = query;
           
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; 
            return pagerowview;

        }

    }
}