﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Finalproject
{
    //Reference from class example

    public partial class _Default : Page
    {
        private string pagequery = "SELECT pageid,pagetitle AS TITLE,pageauthor AS AUTHOR,publishdate AS PUBLISHDATE FROM pagedata_base";
        protected void Page_Load(object sender, EventArgs e)
        {
           page_select.SelectCommand = pagequery;
            page_query.InnerHtml = pagequery;
            page_list.DataSource = Page_Manual_Bind(page_select);
            page_list.DataBind();

            
        }
        protected void Search_page(object sender, EventArgs e)
        {
            string newpagequery = pagequery + " WHERE (1=1) ";
            string searchkey = search_page_title.Text;

            if (searchkey != "")
            {
                newpagequery += " AND   ( pagetitle  LIKE  '%" + searchkey + "%')  ";
            }

            page_select.SelectCommand = newpagequery;
            page_query.InnerHtml = newpagequery;
            page_list.DataSource = Page_Manual_Bind(page_select);
            page_list.DataBind();

        }
        protected DataView Page_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

           

            foreach (DataRow row in mytbl.Rows)
            {

               row["TITLE"] =
                    "<a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["TITLE"]
                    + "</a>";
                row["AUTHOR"] = row["AUTHOR"];
                row["PUBLISHDATE"] = row["PUBLISHDATE"];
            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
        }
    }
}