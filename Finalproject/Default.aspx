﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Finalproject._Default" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <div class="row">
       <div class="col-md-2 col-xs-12"> 
        
             <label>Search page title</label>
           </div>
            <div class="col-md-10 col-xs-12">
            <asp:TextBox runat="server" id="search_page_title"></asp:TextBox><br/ >
            <asp:Button runat="server" Text="SEARCH" OnClick="Search_page"></asp:Button> 
           </div>
        <asp:SqlDataSource 
        runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
       </asp:SqlDataSource>

        <div id="page_query"  runat="server">
    </div>
        <h2>Manage page</h2>
            <a href="Newpage.aspx">ADD NEW PAGE</a>
    

    <asp:DataGrid id="page_list" skinID="dataGrid"
        runat="server" >
    </asp:DataGrid>
     
    </div>

</asp:Content>
