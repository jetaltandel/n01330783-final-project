﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Newpage.aspx.cs" Inherits="Finalproject.Newpage" %>
<asp:Content ID="newPageContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:SqlDataSource runat="server" id="insert_page"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">

    </asp:SqlDataSource>

     <div class="row">
       <div class="col-md-2 col-xs-12"> <label>Page Title:</label></div>
        <div class="col-md-10 col-xs-12">
         <asp:TextBox ID="page_name" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_name"
            ErrorMessage="Enter page title">
        </asp:RequiredFieldValidator>
    </div>
   </div>

    <div class="row">
       <div class="col-md-2 col-xs-12"> <label>Page Content</label></div>
        <div class="col-md-10 col-xs-12">
        <asp:TextBox runat="server" TextMode="multiline" Columns="60" Rows="6" ID="page_content"></asp:TextBox>
    </div>
   </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label> Select Page Author</label></div>
        <div class="col-md-10 col-xs-12">
        <asp:DropDownList ID="page_author" runat="server">
                <asp:ListItem>-Choose One-</asp:ListItem>
                <asp:ListItem>Jetal</asp:ListItem>
                <asp:ListItem>Jinish</asp:ListItem>
                <asp:ListItem>Subham</asp:ListItem>
                <asp:ListItem>other</asp:ListItem>
            </asp:DropDownList>
    </div>

    
    <ASP:Button Text="Add Page" runat="server" OnClick="Addpage" />
    <div runat="server" id="page_query" ></div>

</asp:Content>
