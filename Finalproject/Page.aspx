﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="Finalproject.Page" %>
<asp:Content ID="pyaramidInfo" ContentPlaceHolderID="MainContent" runat="server">


    
     <h1 id="page_title" runat="server"></h1>
    <div id="page_query"  runat="server">
    </div>
    <asp:SqlDataSource runat="server"
        id="page_content"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <div id="page_info" runat="server"></div>

    <asp:SqlDataSource 
        runat="server"
        id="del_page_data"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>

    <asp:Button runat="server" id="del_page"
        OnClick="DeletePage"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />

    <a href="EditPage.aspx?Pageid=<%Response.Write(this.pageid);%>">EDIT</a>
    <div id="page_del_query" runat="server"></div>
    
</asp:Content>
