﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Finalproject
{
    public partial class Newpage : System.Web.UI.Page
    {
        private string addquery = "INSERT INTO pagedata_base(pageid,pagetitle,pagecontent,pageauthor,publishdate) VALUES";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Addpage(object sender, EventArgs e)
        {
            string pageTitle = page_name.Text.ToString();
            string pageContent = page_content.Text.ToString();
            string selectedAuthor = page_author.SelectedValue.ToString();
            string pbldate= DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");


            addquery += "((select max(pageid) from pagedata_base)+1,'" +
                 pageTitle + "','" + pageContent+"','"+ selectedAuthor + "','" + pbldate + "')";

            page_query.InnerHtml = addquery;

           
            insert_page.InsertCommand = addquery;
            insert_page.Insert();
        }
    }
}