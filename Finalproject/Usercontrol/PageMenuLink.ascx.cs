﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Finalproject.Usercontrol
{
    public partial class PageMenuLink : System.Web.UI.UserControl
    {
        private string basequery = "SELECT pageid,pagetitle FROM pagedata_base";

        
        protected  void Page_Load(object sender, EventArgs e)
        { 
            page_lists.SelectCommand = basequery;
            string  new_links= Pagelinks_Manual_Bind(page_lists);
            page_links.InnerHtml = new_links;
        }
        private string Pagelinks_Manual_Bind(SqlDataSource src)
        {

            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);
            string data = string.Empty;
            foreach (DataRowView row in myview)
            {
                string menu_link = "<li><a href= Page.aspx?pageid=" + row["pageid"] + ">" + row["pagetitle"].ToString() + "</a></li>";
                data += menu_link;
                
            }
            return data;




        }
    }
}