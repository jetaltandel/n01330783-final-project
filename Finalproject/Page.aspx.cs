﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Finalproject
{
    public partial class Page : System.Web.UI.Page
    {
        private string page = "SELECT pageid, pagetitle, pagecontent FROM pagedata_base ";

        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (pageid == "" || pageid == null)
            {
                page_title.InnerHtml = "No page found.";
                return;
            }
            else
            {
                page += "  WHERE PAGEID =  " + pageid;

                page_content.SelectCommand = page;
                page_query.InnerHtml = page;

                DataView pageview = (DataView)page_content.Select(DataSourceSelectArguments.Empty);
                DataRowView pagerowview = pageview[0];
                string pagename = pageview[0]["pagetitle"].ToString();
                page_title.InnerHtml = pagename;

                DataView view = (DataView)page_content.Select(DataSourceSelectArguments.Empty);
                DataRowView rowview = view[0];
                string name = view[0]["pagecontent"].ToString();
                page_info.InnerHtml = name;
            }

        }

            protected void DeletePage(object sender, EventArgs e)
            {

                string pagedelquery = "DELETE FROM pagedata_base WHERE pageid=" + pageid;

                page_del_query.InnerHtml = pagedelquery;
                del_page_data.DeleteCommand = pagedelquery;
                del_page_data.Delete();


            }
        

    }
}