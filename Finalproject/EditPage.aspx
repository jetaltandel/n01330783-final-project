﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="Finalproject.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="pageContent">Edit page content</h3>
    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">

    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">

    </asp:SqlDataSource>

    
    <div class="row">
    <div class="col-md-2 col-xs-12"><label>PageTitle:</label></div>
    <div class="col-md-10 col-xs-12">
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title"
            ErrorMessage="Enter a page title">
        </asp:RequiredFieldValidator>
    </div>
    <div>
  
   <div class="row">
   <div class="col-md-2 col-xs-12"><label>PageContent:</label></div>
       <div class="col-md-10 col-xs-12">
        <asp:TextBox ID="page_content" TextMode="multiline" Columns="60" Rows="6" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_content"
            ErrorMessage="Enter a page content">
        </asp:RequiredFieldValidator>
    </div>
    </div>
        <div class="row">
    <ASP:Button Text="Edit Page" runat="server" onClick="Edit_Page"/>
    </div>


</asp:Content>
